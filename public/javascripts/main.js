$(function () {

    $("#slide-div").hide()


    var pushStories = function (stories) {
        for (var i = 0; i < stories.length; i++) {

            var $story = $("#story-hidden .story").clone()

            $story.find(".story-username").html(stories[i].name)
            $story.find(".story-body").html(stories[i].body)

            var $content = $story.find(".story-content")

            $story.hide()
            $content.hide()

            $("#storyBox").prepend($story)
            $story.slideDown(500, 'swing', function() {
                $content.fadeIn("slow")
            })

        }
    }

    var getStories = function () {
        console.log("getting stories")

        $.ajax({
                url:"/stories.json",
                success:function (stories) {
                    pushStories(stories)
                }
            }
        )
    }

    setInterval(getStories, 1000)
})
