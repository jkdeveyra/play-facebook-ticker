package controllers

import play.api._
import play.api.mvc._
import models._
import util.Random
import views._

object Application extends Controller {

  def index = Action {
    Ok(html.index())
  }

  def stories = Action {
    val stories = randomStories(1)

    Ok(asJson(stories)).as("application/json")
  }

  private def asJson(stories: List[Story]) =
    stories.map(_.toJson).mkString("[", ",", "]")

  private def randomStory = {
    val jk = User("JK de Veyra", Male)
    val gilbert = User("Gilbert Carilla", Male)
    val jed = User("Jed Allado", Male)
    val jojo = User("Antonio Enaje", Male)
    val users = jk :: gilbert :: jed :: jojo :: Nil

    val actions = Update :: Comment :: Like :: Share :: Nil
    val resources = Photo :: RelationshipStatus :: StatusUpdate :: Nil
    val ruser = users(Random.nextInt(users.length))
    val raction = actions(Random.nextInt(actions.length))
    val rres = resources(Random.nextInt(resources.length))

    Story(ruser, raction, rres)
  }

  private def randomStories(n: Int = 10): List[Story] = {
    (for (i <- 0 until n) yield randomStory) toList
  }
}