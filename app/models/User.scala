package models

case class Gender(name: String, pronoun: String)

object Male extends Gender("Male", "his")

object Female extends Gender("Female", "her")


case class User(name: String, gender: Gender) {
  def pronoun = gender.pronoun
}

case class UserAction(detail: String) {
  override def toString = detail
}

object Like extends UserAction("like")

object Share extends UserAction("shared")

object Update extends UserAction("updated")

object Comment extends UserAction("commented")


case class Resource(name: String)

object Photo extends Resource("photo")

object RelationshipStatus extends Resource("relationship status")

object StatusUpdate extends Resource("status update")


case class Story(user: User, action: UserAction, resource: Resource) {

  def body = action.detail + " " + user.pronoun + " " + resource.name + "."

  def toJson = "{ \"name\": \"%s\", \"body\": \"%s\" }" format(user.name, body)

}
